SHELL := /bin/bash
NAME := origin-vendoring-playground

GO := GO15VENDOREXPERIMENT=1 go
GO_VERSION := $(shell $(GO) version | sed -e 's/^[^0-9.]*\([0-9.]*\).*/\1/')

PREFIX?=$(shell pwd)
ROOT_PACKAGE := $(shell $(GO) list .)
GOFILES=$(shell find . -type f -name '*.go')
VENDOR_DIR=vendor

VERSION := $(shell cat VERSION)
REV := $(shell git rev-parse --short HEAD 2> /dev/null  || echo 'unknown')
BRANCH := $(shell git rev-parse --abbrev-ref HEAD 2> /dev/null  || echo 'unknown')
BUILD_DATE := $(shell date -u +%Y-%m-%dT%H:%M:%SZ)
BUILDFLAGS := -ldflags \
  " -X $(ROOT_PACKAGE)/version.Version=$(VERSION)\
		-X $(ROOT_PACKAGE)/version.Revision='$(REV)'\
		-X $(ROOT_PACKAGE)/version.Branch='$(BRANCH)'\
		-X $(ROOT_PACKAGE)/version.BuildDate='$(BUILD_DATE)'\
		-X $(ROOT_PACKAGE)/version.GoVersion='$(GO_VERSION)'"
CGO_ENABLED=0
GO15VENDOREXPERIMENT=1


.PHONY: oe2k build-finder start-build clean
.DEFAULT: all

all: ${PREFIX}/bin/oe2k ${PREFIX}/bin/build-finder ${PREFIX}/bin/buildlog2hdfs ${PREFIX}/bin/rest-gw ${PREFIX}/bin/git-enricher

${PREFIX}/bin/oe2k: $(GOFILES)
	@echo "building $@"
	@go build -o $@ ${GO_LDFLAGS} ${GO_GCFLAGS} ./cmd/oe2k

${PREFIX}/bin/build-finder: $(GOFILES)
	@echo "building $@"
	@go build -o $@ ${GO_LDFLAGS} ${GO_GCFLAGS} ./cmd/buildfinder

${PREFIX}/bin/buildlog2hdfs: $(GOFILES)
	@echo "building $@"
	@go build -o $@ ${GO_LDFLAGS} ${GO_GCFLAGS} ./cmd/log2hdfs

${PREFIX}/bin/rest-gw: $(GOFILES)
	@echo "building $@"
	@go build -o $@ ${GO_LDFLAGS} ${GO_GCFLAGS} ./cmd/rest-gw

${PREFIX}/bin/git-enricher: $(GOFILES)
	@echo "building $@"
	@go build -o $@ ${GO_LDFLAGS} ${GO_GCFLAGS} ./cmd/git-enricher

deps:
	@govendor sync

vendoring:
	GO15VENDOREXPERIMENT=1 glide update --strip-vendor --strip-vcs

start-oe2k-build: ${PREFIX}/bin/oe2k
	cp Dockerfile.oe2k Dockerfile
	@oc start-build oe2k-binary --from-dir=. --follow
	@rm Dockerfile

start-build-finder-build: ${PREFIX}/bin/build-finder
	cp Dockerfile.build-finder Dockerfile
	@oc start-build build-finder-binary --from-dir=. --follow
	@rm Dockerfile

start-buildlog2hdfs-build: ${PREFIX}/bin/buildlog2hdfs
	cp Dockerfile.buildlog2hdfs Dockerfile
	@oc start-build buildlog2hdfs-binary --from-dir=. --follow
	@rm Dockerfile

start-rest-gw-build: ${PREFIX}/bin/rest-gw
	cp Dockerfile.rest-gw Dockerfile
	@oc start-build rest-gw-binary --from-dir=. --follow
	@rm Dockerfile

start-git-enricher-build: ${PREFIX}/bin/git-enricher
	cp Dockerfile.git-enricher Dockerfile
	@oc start-build git-enricher-binary --from-dir=. --follow
	@rm Dockerfile

clean:
	@echo "Cleaning up version ${VERSION} ..."
	@rm -f ${PREFIX}/bin/oe2k ${PREFIX}/bin/build-finder ${PREFIX}/bin/buildlog2hdfs ${PREFIX}/bin/rest-gw ${PREFIX}/bin/git-enricher


