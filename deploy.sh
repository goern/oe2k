#!/bin/bash

# Project and Permissions
oc new-project analytics
oc login -u system:admin
oc create sa hadoop
oc adm policy add-cluster-role-to-user admin system:serviceaccount:analytics:default # to see all pods in all namespaces
oc adm policy add-scc-to-user anyuid -z hadoop # to use root uid for execution

# Basics
oc import-image docker.io/goern/go-17-centos7 --confirm
oc import-image registry.access.redhat.com/rhel7-atomic --confirm

# HDFS namenode
oc create -f https://gitlab.com/goern/hdfs-openshift/raw/master/namenode.yaml
oc create service loadbalancer hdfs-namenode --tcp=8020:8020 --tcp=50070:50070
oc expose service hdfs-namenode --port=50070

# Apache Kafka
oc create -f https://gitlab.com/goern/openshift-kafka/raw/master/resources.yaml
oc new-app --template apache-kafka

# OE2K
oc new-build --image-stream=rhel7-atomic --binary=true --name=oe2k-binary --to=oe2k
make start-oe2k-build
oc new-app --allow-missing-images --image-stream=oe2k --allow-missing-imagestream-tags \
 --env=OE2K_KAFKA_PEERS=apache-kafka:9092 --env=KAFKA_PEERS=apache-kafka:9092 

# build-finder
oc new-build --image-stream=rhel7-atomic --binary=true --name=build-finder-binary --to=build-finder
make start-build-finder-build
oc new-app --allow-missing-images --image-stream=build-finder --allow-missing-imagestream-tags \
 --env=OE2K_KAFKA_PEERS=apache-kafka:9092 --env=OE2K_BUILD_FINDER_PORT=8080
oc expose service build-finder

# git-enricher
oc new-build --image-stream=rhel7-atomic --binary=true --name=git-enricher-binary --to=git-enricher
make start-git-enricher-build
oc new-app --allow-missing-images --image-stream=git-enricher --allow-missing-imagestream-tags \
 --env=OE2K_KAFKA_PEERS=apache-kafka:9092 --env=OE2K_MY_PORT=8080
oc expose service git-enricher

# rest-gw
oc new-build --image-stream=rhel7-atomic --binary=true --name=rest-gw-binary --to=rest-gw
make start-rest-gw-build
oc new-app --allow-missing-images --image-stream=rest-gw --allow-missing-imagestream-tags \
 --env=OE2K_KAFKA_PEERS=apache-kafka:9092 --env=OE2K_REST_API_PORT=8080
oc expose service rest-gw

# HDFS datanode
oc create -f https://gitlab.com/goern/hdfs-openshift/raw/master/datanode.yaml

# buildlog2hdfs
oc new-build --image-stream=rhel7-atomic --binary=true --name=buildlog2hdfs-binary --to=buildlog2hdfs
make start-buildlog2hdfs-build
oc new-app --allow-missing-images --image-stream=buildlog2hdfs --allow-missing-imagestream-tags \
 --env=OE2K_KAFKA_PEERS=apache-kafka:9092 --env=OE2K_HDFS_NAMENODE=hdfs-namenode:8020 --env=HADOOP_USER_NAME=hadoop
oc expose service buildlog2hdfs
