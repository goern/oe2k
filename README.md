# OpenShift Events to Kafka

This thing will relay OpenShift/Kubernetes events to a Kafka topic. For now we only do Pod events in namespace 'myproject'

## Deploy

To deploy this proof of concept bring up a Kafka by `oc create -f https://gitlab.com/goern/openshift-kafka/raw/master/resources.yaml && oc new-app --template apache-kafka`

OUTDATED: Next thing is to get a golang s2i toolchain: `oc import-image --confirm goern/go-17-centos7` and create a new build configuration: `oc new-build go-17-centos7~https://goern@gitlab.com/goern/oe2k.git`

The final step is to create a new application from the image stream containing the build result: `oc new-app --allow-missing-images --image-stream=oe2k --allow-missing-imagestream-tags --env=KAFKA_PEERS=apache-kafka:9092 --env=PORT=8080`

## Usage

`oc status` will let you know when everything is deployed. If you have a look at the log of the oe2k pod you will see lots of errors, the user cant list pods in the 'myproject'. To fix this grant a little bit more of permissions: `oc adm policy add-cluster-role-to-user admin system:serviceaccount:myproject:default`

Some metrics are exposed for Prometheus at `:8080/metrics`, this requires a `oc expose service oe2k`

### Consume

To see all the messages coming in to Kafka you need to `oc rsh <apache-kafka-pod-name>` and use `bin/kafka-console-consumer.sh --bootstrap-server apache-kafka:9092 --topic openshift_events --from-beginning`

#### Build-Finder

There is one component called build-finder, it will look out for Pods that terminated successful and if it was a build, it will pass that information on to the next topic: `openshift_builds_discovered`. To deploy build-finder: 

```
oc new-build --image-stream=rhel7-atomic --binary=true --name=build-finder-binary --to=build-finder
make start-build-finder-build
oc new-app --allow-missing-images --image-stream=build-finder --allow-missing-imagestream-tags --env=OE2K_KAFKA_PEERS=apache-kafka:9092 --env=OE2K_BUILD_FINDER_PORT=8080
oc expose service build-finder
```

#### buildlog2hdfs

Prerequisites: you need a running HDFS cluster, to set one up on OpenShift see http://b4mad.net/datenbrei/openshift/hadoop-hdfs/

Deploying buildlog2hdfs by the following commands will result in a service downloading each buildlog to its local /tmp ;)

```
oc new-build --image-stream=rhel7-atomic --binary=true --name=buildlog2hdfs-binary --to=buildlog2hdfs
make start-buildlog2hdfs-build
oc new-app --allow-missing-images --image-stream=buildlog2hdfs --allow-missing-imagestream-tags --env=OE2K_KAFKA_PEERS=apache-kafka:9092 --env=OE2K_HDFS_NAMENODE=hdfs-namenode:8020 --env=HADOOP_USER_NAME=hadoop
oc expose service buildlog2hdfs
```

## Development

add `oc import-image registry.access.redhat.com/rhel7-atomic --confirm` we will use it as he base for the golang binary. Create a binary build config using `oc new-build --image-stream=rhel7-atomic --binary=true --name=oe2k-binary --to=oe2k`. After `make start-oe2k-build` the binary in your local directory, you see it building and pushing it to the binary build...

As the binary build config has written to ImageStream 'oe2k-binary' we need to tell the deployment to use that ImageStream rather than the one it is using now, use the OpenShift web console :) 

### Apache Kafka Topic

This applicaton uses the following Apache Kafka topics:

|Topic|Description|
|---|---|
|openshift_events|Pod create, delete, update events   |
|openshift_builds_discovered|discovered succeeded OpenShift builds|
|openshift_builds_git_commit_hash|This contains build-git commit information|

## TODO

* check that logrus logs to stdout, so that we can see it on the console
* configure loglevel via ENV: LOG_LEVEL
* use a generic approach so that events from all objects in all namespaces get caught
* healthz handler: include kafka connection status check
* healthz handler: include info if we could talk to kubernetes master
* govendor: remove version pinning of golang.org/x/net/http2
