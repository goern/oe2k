/*
   oe2k
   Copyright (C) 2017 Christoph Görn

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

package oe2k

import (
	"crypto/tls"
	"errors"
	"io/ioutil"
	"net/http"

	log "github.com/Sirupsen/logrus"
)

//GetBearerToken will get a auth token from the filesystem, this assumes we run in an OpenShift cluster
func GetBearerToken() (string, error) {
	b, err := ioutil.ReadFile("/var/run/secrets/kubernetes.io/serviceaccount/token")
	if err != nil {
		log.Fatal(err)

		return "", errors.New("Cant get OpenShift Bearer Token from /var/run/secrets/kubernetes.io/serviceaccount/token")
	}

	return string(b), nil
}

//GetBuildLog will download a named buildlog from OpenShift and return it as a string
func GetBuildLog(namespace string, build string) (*string, error) {
	var bodyString string

	req, err := http.NewRequest("GET", "https://openshift.default.svc.cluster.local/oapi/v1/namespaces/"+namespace+"/builds/"+build+"/log", nil)
	if err != nil {
		log.Error(err)
		return nil, errors.New("Cant get OpenShift BuildLog")
	}

	token, err := GetBearerToken()
	if err != nil {
		log.Error(err)
		return nil, errors.New("Cant get OpenShift BuildLog")
	}

	req.Header.Set("Authorization", "Bearer "+token)
	req.Header.Set("User-Agent", "buildlog2hdfs/v0.0.1 openshift/v1.5 (linux/amd64)")
	req.Header.Set("Accept", "application/json, */*")

	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}

	client := &http.Client{Transport: tr}

	resp, err := client.Do(req)
	if err != nil {
		log.Error(err)
		return nil, errors.New("Cant get OpenShift BuildLog")
	}
	defer resp.Body.Close()

	if resp.StatusCode == 200 { // OK
		bodyBytes, err := ioutil.ReadAll(resp.Body)

		if err != nil {
			log.Error(err)
			return nil, errors.New("Cant get OpenShift BuildLog")
		}

		bodyString = string(bodyBytes)

	}

	return &bodyString, nil
}
