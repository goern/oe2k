/*
   oe2k
   Copyright (C) 2017 Christoph Görn

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package oe2k

import (
	"fmt"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestFindGitCommit(t *testing.T) {
	file, _ := os.Open("../fixtures/nodejs-build.txt")

	hash, err := FindGitCommit(file)
	if err != nil {
		fmt.Println(err)
		assert.FailNow(t, err.Error())
	}

	assert.Equal(t, "3d44de3ba8fef0b2baca4ddd001c0db286ea4cd3", hash, "there is something wrong with the git commit id")
}

func TestFindGitRepo(t *testing.T) {
	file, _ := os.Open("../fixtures/nodejs-build.txt")

	hash, err := FindGitRepository(file)
	if err != nil {
		fmt.Println(err)
		assert.FailNow(t, err.Error())
	}

	assert.Equal(t, "https://github.com/openshift/nodejs-ex.git", hash, "there is something wrong with the git repo")
}
