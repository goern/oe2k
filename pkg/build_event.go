/*
   oe2k
   Copyright (C) 2017 Christoph Görn

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

package oe2k

import (
	"fmt"

	"github.com/google/jsonapi"
	uuid "github.com/satori/go.uuid"
)

//BuildEvent represents an OpenShift Build that has successfully terminated.
type BuildEvent struct {
	UUID                 string `jsonapi:"primary,build_events"`
	Namespace            string `jsonapi:"attr,namespace"`
	SourceRepository     string `jsonapi:"attr,source_repository"`
	SourceCommitHash     string `jsonapi:"attr,source_commit_hash"`
	Build                string `jsonapi:"attr,build"`
	OutputImageStreamTag string `jsonapi:"attr,output_imagestream_tag"`
}

//NewBuildEvent initialized a BuildEvent with a new UUID
func NewBuildEvent() *BuildEvent {
	return &BuildEvent{UUID: uuid.NewV4().String()}
}

// BuildEvent Links
func (b BuildEvent) JSONAPILinks() *jsonapi.Links {
	return &jsonapi.Links{
		"self": fmt.Sprintf("https://example.com/build_events/%s", b.UUID),
	}
}

//BuildSourceCommitHash enriches a BuildEvent with a Source Commit Hash
type BuildSourceCommitHash struct {
	UUID             string `jsonapi:"primary,build_events"`
	SourceRepository string `jsonapi:"attr,source_repository"`
	SourceCommitHash string `jsonapi:"attr,source_commit_hash"`
}
