/*
   oe2k
   Copyright (C) 2017 Christoph Görn

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

package oe2k

import (
	"bytes"
	"errors"
	"io"
	"regexp"
	"strings"

	log "github.com/Sirupsen/logrus"
)

//FindGitCommit will find a git commit hash in the given io.Reader
func FindGitCommit(r io.Reader) (string, error) {
	hashRegexp := regexp.MustCompile(`(?s)\s(Commit)\:.(\w*)(\s)(\w*)`)

	buf := new(bytes.Buffer)

	if _, err := buf.ReadFrom(r); err != nil {
		log.Fatal(err)
		return "", errors.New("No Git commit hash found")
	}

	hashMatches := hashRegexp.FindAllString(buf.String(), -1)

	if len(hashMatches) > 0 {
		return strings.Trim(strings.Split(hashMatches[0], "\t")[2], " "), nil
	}

	return "", errors.New("No Git commit hash found")
}

//FindGitRepository will find a git repository URL in the given io.Reader
func FindGitRepository(r io.Reader) (string, error) {
	repoRegexp := regexp.MustCompile(`Cloning\s+\"([-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?)\"`)

	buf := new(bytes.Buffer)

	if _, err := buf.ReadFrom(r); err != nil {
		log.Fatal(err)
		return "", errors.New("No Git repository found")
	}

	repoMatches := repoRegexp.FindAllString(buf.String(), -1)

	if len(repoMatches) > 0 {
		return stripchars(strings.Split(repoMatches[0], " ")[1], "\\\""), nil
	}

	return "", errors.New("No Git repository found")
}

func stripchars(str, chr string) string {
	return strings.Map(func(r rune) rune {
		if strings.IndexRune(chr, r) < 0 {
			return r
		}
		return -1
	}, str)
}
