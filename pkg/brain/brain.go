/*
   oe2k
   Copyright (C) 2017 Christoph Görn

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

package brain

import (
	"github.com/deckarep/golang-set"
	oe2k "gitlab.com/goern/oe2k/pkg"
)

type InMemoryBrain struct {
	Builds mapset.Set
}

func NewInMemoryBrain() *InMemoryBrain {
	brain := InMemoryBrain{}
	brain.Builds = mapset.NewSet()

	return &brain
}

func (b *InMemoryBrain) AddBuild(build *oe2k.BuildEvent) {
	b.Builds.Add(build)
}

func (b *InMemoryBrain) GetBuild(uuid string) *oe2k.BuildEvent {
	var found *oe2k.BuildEvent
	it := b.Builds.Iterator()

	for elem := range it.C {
		if elem.(*oe2k.BuildEvent).UUID == uuid {
			found = elem.(*oe2k.BuildEvent)
			it.Stop()
		}
	}

	return found
}

func (b *InMemoryBrain) SetSourceCommitHash(uuid string, hash string) {
	elem := b.GetBuild(uuid)

	if elem != nil {
		elem.SourceCommitHash = hash
	}
}

func (b *InMemoryBrain) SetSourceRepository(uuid string, repo string) {
	elem := b.GetBuild(uuid)

	if elem != nil {
		elem.SourceRepository = repo
	}
}
