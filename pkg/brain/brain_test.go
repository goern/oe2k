/*
   oe2k
   Copyright (C) 2017 Christoph Görn

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

package brain

import (
	"testing"

	oe2k "gitlab.com/goern/oe2k/pkg"

	"github.com/stretchr/testify/assert"
)

func TestNewInMemoryBrain(t *testing.T) {
	builds := NewInMemoryBrain()
	assert.NotNil(t, builds, "builds <<InMemoryBrain>> should not be nil!")

	b1 := builds.GetBuild("a")
	assert.Nil(t, b1, "There should be nothing by UUID 'a'")

	builds.AddBuild(&oe2k.BuildEvent{UUID: "a", Namespace: "test", Build: "test-1"})
	b1 = builds.GetBuild("a")
	assert.NotNil(t, b1, "There should be nothing by UUID 'a'")

	builds.SetSourceCommitHash("a", "1234")
	b1 = builds.GetBuild("a")
	assert.Equal(t, "1234", b1.SourceCommitHash)

}
