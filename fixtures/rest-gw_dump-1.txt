{
    "data": [
        {
            "type": "build_events",
            "id": "8cf5167c-a960-4932-be07-380bcf29d354",
            "attributes": {
                "build": "nodejs-ex-1",
                "namespace": "testing-1",
                "output_imagestream_tag": "172.30.1.1:5000/testing-1/nodejs-ex:latest",
                "source_commit_hash": "3d44de3ba8fef0b2baca4ddd001c0db286ea4cd3",
                "source_repository": "https://github.com/openshift/nodejs-ex.git"
            },
            "links": {
                "self": "https://example.com/build_events/8cf5167c-a960-4932-be07-380bcf29d354"
            }
        },
        {
            "type": "build_events",
            "id": "9ba6f319-cedb-401b-836e-1fa67ec2beaa",
            "attributes": {
                "build": "rest-gw-binary-2",
                "namespace": "analytics",
                "output_imagestream_tag": "172.30.1.1:5000/analytics/rest-gw:latest",
                "source_commit_hash": "",
                "source_repository": ""
            },
            "links": {
                "self": "https://example.com/build_events/9ba6f319-cedb-401b-836e-1fa67ec2beaa"
            }
        },
        {
            "type": "build_events",
            "id": "2bf63532-8833-479f-a745-2a05900eb202",
            "attributes": {
                "build": "rest-gw-binary-3",
                "namespace": "analytics",
                "output_imagestream_tag": "172.30.1.1:5000/analytics/rest-gw:latest",
                "source_commit_hash": "",
                "source_repository": ""
            },
            "links": {
                "self": "https://example.com/build_events/2bf63532-8833-479f-a745-2a05900eb202"
            }
        },
        {
            "type": "build_events",
            "id": "e8d48d26-2e8d-4465-b123-3f2aba8838b9",
            "attributes": {
                "build": "rest-gw-binary-5",
                "namespace": "analytics",
                "output_imagestream_tag": "172.30.1.1:5000/analytics/rest-gw:latest",
                "source_commit_hash": "",
                "source_repository": ""
            },
            "links": {
                "self": "https://example.com/build_events/e8d48d26-2e8d-4465-b123-3f2aba8838b9"
            }
        },
        {
            "type": "build_events",
            "id": "ceddc776-539c-4272-9efa-74a41524c37f",
            "attributes": {
                "build": "rest-gw-binary-8",
                "namespace": "analytics",
                "output_imagestream_tag": "172.30.1.1:5000/analytics/rest-gw:latest",
                "source_commit_hash": "",
                "source_repository": ""
            },
            "links": {
                "self": "https://example.com/build_events/ceddc776-539c-4272-9efa-74a41524c37f"
            }
        },
        {
            "type": "build_events",
            "id": "8d4eec7a-fb42-4666-a1a0-e494ea450364",
            "attributes": {
                "build": "build-finder-binary-16",
                "namespace": "myproject",
                "output_imagestream_tag": "172.30.1.1:5000/myproject/build-finder:latest",
                "source_commit_hash": "",
                "source_repository": ""
            },
            "links": {
                "self": "https://example.com/build_events/8d4eec7a-fb42-4666-a1a0-e494ea450364"
            }
        },
        {
            "type": "build_events",
            "id": "17bf0174-5ebe-4e8a-9e99-1f76453905a2",
            "attributes": {
                "build": "build-finder-binary-12",
                "namespace": "myproject",
                "output_imagestream_tag": "172.30.1.1:5000/myproject/build-finder:latest",
                "source_commit_hash": "",
                "source_repository": ""
            },
            "links": {
                "self": "https://example.com/build_events/17bf0174-5ebe-4e8a-9e99-1f76453905a2"
            }
        },
        {
            "type": "build_events",
            "id": "a4a21257-e9ab-47aa-9c85-69165a94778a",
            "attributes": {
                "build": "oe2k-binary-2",
                "namespace": "myproject",
                "output_imagestream_tag": "172.30.1.1:5000/myproject/oe2k:latest",
                "source_commit_hash": "",
                "source_repository": ""
            },
            "links": {
                "self": "https://example.com/build_events/a4a21257-e9ab-47aa-9c85-69165a94778a"
            }
        },
        {
            "type": "build_events",
            "id": "ed7e7936-e34c-40cb-ad77-ea3aa375639b",
            "attributes": {
                "build": "rest-gw-binary-16",
                "namespace": "analytics",
                "output_imagestream_tag": "172.30.1.1:5000/analytics/rest-gw:latest",
                "source_commit_hash": "",
                "source_repository": ""
            },
            "links": {
                "self": "https://example.com/build_events/ed7e7936-e34c-40cb-ad77-ea3aa375639b"
            }
        },
        {
            "type": "build_events",
            "id": "f30baf55-4927-4722-9f9e-4d85bf4742c8",
            "attributes": {
                "build": "rest-gw-binary-9",
                "namespace": "analytics",
                "output_imagestream_tag": "172.30.1.1:5000/analytics/rest-gw:latest",
                "source_commit_hash": "",
                "source_repository": ""
            },
            "links": {
                "self": "https://example.com/build_events/f30baf55-4927-4722-9f9e-4d85bf4742c8"
            }
        },
        {
            "type": "build_events",
            "id": "3556e05d-296e-486f-9bba-0570d6af1e20",
            "attributes": {
                "build": "oe2k-binary-8",
                "namespace": "myproject",
                "output_imagestream_tag": "172.30.1.1:5000/myproject/oe2k:latest",
                "source_commit_hash": "",
                "source_repository": ""
            },
            "links": {
                "self": "https://example.com/build_events/3556e05d-296e-486f-9bba-0570d6af1e20"
            }
        },
        {
            "type": "build_events",
            "id": "aa2b0359-9bc0-48d8-bf82-cfbd1be83e53",
            "attributes": {
                "build": "build-finder-binary-15",
                "namespace": "myproject",
                "output_imagestream_tag": "172.30.1.1:5000/myproject/build-finder:latest",
                "source_commit_hash": "",
                "source_repository": ""
            },
            "links": {
                "self": "https://example.com/build_events/aa2b0359-9bc0-48d8-bf82-cfbd1be83e53"
            }
        },
        {
            "type": "build_events",
            "id": "bd15f29b-23d3-4248-be67-954600e844a3",
            "attributes": {
                "build": "rest-gw-binary-1",
                "namespace": "analytics",
                "output_imagestream_tag": "172.30.1.1:5000/analytics/rest-gw:latest",
                "source_commit_hash": "",
                "source_repository": ""
            },
            "links": {
                "self": "https://example.com/build_events/bd15f29b-23d3-4248-be67-954600e844a3"
            }
        },
        {
            "type": "build_events",
            "id": "9223285f-1fe6-40db-93b4-17abba028a4b",
            "attributes": {
                "build": "build-finder-binary-5",
                "namespace": "myproject",
                "output_imagestream_tag": "172.30.1.1:5000/myproject/build-finder:latest",
                "source_commit_hash": "",
                "source_repository": ""
            },
            "links": {
                "self": "https://example.com/build_events/9223285f-1fe6-40db-93b4-17abba028a4b"
            }
        },
        {
            "type": "build_events",
            "id": "faca8b46-072c-482f-86fd-6efba6e7d8d6",
            "attributes": {
                "build": "rest-gw-binary-6",
                "namespace": "myproject",
                "output_imagestream_tag": "172.30.1.1:5000/myproject/rest-gw:latest",
                "source_commit_hash": "",
                "source_repository": ""
            },
            "links": {
                "self": "https://example.com/build_events/faca8b46-072c-482f-86fd-6efba6e7d8d6"
            }
        },
        {
            "type": "build_events",
            "id": "11a87b51-d781-4a8e-b5b3-cab54c05b0ec",
            "attributes": {
                "build": "build-finder-binary-7",
                "namespace": "myproject",
                "output_imagestream_tag": "172.30.1.1:5000/myproject/build-finder:latest",
                "source_commit_hash": "",
                "source_repository": ""
            },
            "links": {
                "self": "https://example.com/build_events/11a87b51-d781-4a8e-b5b3-cab54c05b0ec"
            }
        },
        {
            "type": "build_events",
            "id": "f1f1638e-dc86-48fb-a8ce-c2578e8eae5a",
            "attributes": {
                "build": "git-commit-finder-binary-5",
                "namespace": "analytics",
                "output_imagestream_tag": "172.30.1.1:5000/analytics/git-commit-finder:latest",
                "source_commit_hash": "",
                "source_repository": ""
            },
            "links": {
                "self": "https://example.com/build_events/f1f1638e-dc86-48fb-a8ce-c2578e8eae5a"
            }
        },
        {
            "type": "build_events",
            "id": "079da6a9-5214-411c-8718-04e6577e595c",
            "attributes": {
                "build": "rest-gw-binary-17",
                "namespace": "analytics",
                "output_imagestream_tag": "172.30.1.1:5000/analytics/rest-gw:latest",
                "source_commit_hash": "",
                "source_repository": ""
            },
            "links": {
                "self": "https://example.com/build_events/079da6a9-5214-411c-8718-04e6577e595c"
            }
        },
        {
            "type": "build_events",
            "id": "127c17e6-0f4f-43b0-b39f-e7e66525c07d",
            "attributes": {
                "build": "oe2k-binary-6",
                "namespace": "myproject",
                "output_imagestream_tag": "172.30.1.1:5000/myproject/oe2k:latest",
                "source_commit_hash": "",
                "source_repository": ""
            },
            "links": {
                "self": "https://example.com/build_events/127c17e6-0f4f-43b0-b39f-e7e66525c07d"
            }
        },
        {
            "type": "build_events",
            "id": "494454ce-8141-4673-8036-f4bff93cd448",
            "attributes": {
                "build": "build-finder-binary-18",
                "namespace": "myproject",
                "output_imagestream_tag": "172.30.1.1:5000/myproject/build-finder:latest",
                "source_commit_hash": "",
                "source_repository": ""
            },
            "links": {
                "self": "https://example.com/build_events/494454ce-8141-4673-8036-f4bff93cd448"
            }
        },
        {
            "type": "build_events",
            "id": "cde8e47d-cdba-480b-9883-0453468bbb62",
            "attributes": {
                "build": "oe2k-binary-1",
                "namespace": "analytics",
                "output_imagestream_tag": "172.30.1.1:5000/analytics/oe2k:latest",
                "source_commit_hash": "",
                "source_repository": ""
            },
            "links": {
                "self": "https://example.com/build_events/cde8e47d-cdba-480b-9883-0453468bbb62"
            }
        },
        {
            "type": "build_events",
            "id": "e3f521d0-f0ec-4664-afc4-803750b48df6",
            "attributes": {
                "build": "build-finder-binary-4",
                "namespace": "myproject",
                "output_imagestream_tag": "172.30.1.1:5000/myproject/build-finder:latest",
                "source_commit_hash": "",
                "source_repository": ""
            },
            "links": {
                "self": "https://example.com/build_events/e3f521d0-f0ec-4664-afc4-803750b48df6"
            }
        },
        {
            "type": "build_events",
            "id": "b538fd4e-3d41-4381-a4ee-d9c266b26df7",
            "attributes": {
                "build": "build-finder-binary-11",
                "namespace": "myproject",
                "output_imagestream_tag": "172.30.1.1:5000/myproject/build-finder:latest",
                "source_commit_hash": "",
                "source_repository": ""
            },
            "links": {
                "self": "https://example.com/build_events/b538fd4e-3d41-4381-a4ee-d9c266b26df7"
            }
        },
        {
            "type": "build_events",
            "id": "9614fce4-88c3-43eb-813d-e95357685a56",
            "attributes": {
                "build": "rest-gw-binary-4",
                "namespace": "myproject",
                "output_imagestream_tag": "172.30.1.1:5000/myproject/rest-gw:latest",
                "source_commit_hash": "",
                "source_repository": ""
            },
            "links": {
                "self": "https://example.com/build_events/9614fce4-88c3-43eb-813d-e95357685a56"
            }
        },
        {
            "type": "build_events",
            "id": "611abba5-f7a6-47ec-ad64-540158f2d5a4",
            "attributes": {
                "build": "rest-gw-binary-14",
                "namespace": "analytics",
                "output_imagestream_tag": "172.30.1.1:5000/analytics/rest-gw:latest",
                "source_commit_hash": "",
                "source_repository": ""
            },
            "links": {
                "self": "https://example.com/build_events/611abba5-f7a6-47ec-ad64-540158f2d5a4"
            }
        },
        {
            "type": "build_events",
            "id": "e54b6887-5b36-49c4-8a87-cf6be25f3969",
            "attributes": {
                "build": "buildlog2hdfs-binary-1",
                "namespace": "myproject",
                "output_imagestream_tag": "172.30.1.1:5000/myproject/buildlog2hdfs:latest",
                "source_commit_hash": "",
                "source_repository": ""
            },
            "links": {
                "self": "https://example.com/build_events/e54b6887-5b36-49c4-8a87-cf6be25f3969"
            }
        },
        {
            "type": "build_events",
            "id": "8deefb41-56ad-493f-8f56-330f041ec83d",
            "attributes": {
                "build": "build-finder-binary-8",
                "namespace": "myproject",
                "output_imagestream_tag": "172.30.1.1:5000/myproject/build-finder:latest",
                "source_commit_hash": "",
                "source_repository": ""
            },
            "links": {
                "self": "https://example.com/build_events/8deefb41-56ad-493f-8f56-330f041ec83d"
            }
        },
        {
            "type": "build_events",
            "id": "edf088e8-e28d-44e1-a8d4-4bfa9ca863fa",
            "attributes": {
                "build": "build-finder-binary-9",
                "namespace": "myproject",
                "output_imagestream_tag": "172.30.1.1:5000/myproject/build-finder:latest",
                "source_commit_hash": "",
                "source_repository": ""
            },
            "links": {
                "self": "https://example.com/build_events/edf088e8-e28d-44e1-a8d4-4bfa9ca863fa"
            }
        },
        {
            "type": "build_events",
            "id": "79683cdd-6017-40a7-8ec0-4e9af17ee367",
            "attributes": {
                "build": "git-commit-finder-binary-2",
                "namespace": "analytics",
                "output_imagestream_tag": "172.30.1.1:5000/analytics/git-commit-finder:latest",
                "source_commit_hash": "",
                "source_repository": ""
            },
            "links": {
                "self": "https://example.com/build_events/79683cdd-6017-40a7-8ec0-4e9af17ee367"
            }
        },
        {
            "type": "build_events",
            "id": "296af7e6-b99f-429a-a64e-00eca5dafb7b",
            "attributes": {
                "build": "rest-gw-binary-6",
                "namespace": "analytics",
                "output_imagestream_tag": "172.30.1.1:5000/analytics/rest-gw:latest",
                "source_commit_hash": "",
                "source_repository": ""
            },
            "links": {
                "self": "https://example.com/build_events/296af7e6-b99f-429a-a64e-00eca5dafb7b"
            }
        },
        {
            "type": "build_events",
            "id": "d3c1c2a6-1ab8-4ea2-ba34-039e566ac802",
            "attributes": {
                "build": "rest-gw-binary-5",
                "namespace": "myproject",
                "output_imagestream_tag": "172.30.1.1:5000/myproject/rest-gw:latest",
                "source_commit_hash": "",
                "source_repository": ""
            },
            "links": {
                "self": "https://example.com/build_events/d3c1c2a6-1ab8-4ea2-ba34-039e566ac802"
            }
        },
        {
            "type": "build_events",
            "id": "ec59d782-6722-4f79-9ebd-adf09e75bd9f",
            "attributes": {
                "build": "rest-gw-binary-7",
                "namespace": "myproject",
                "output_imagestream_tag": "172.30.1.1:5000/myproject/rest-gw:latest",
                "source_commit_hash": "",
                "source_repository": ""
            },
            "links": {
                "self": "https://example.com/build_events/ec59d782-6722-4f79-9ebd-adf09e75bd9f"
            }
        },
        {
            "type": "build_events",
            "id": "e47b77ad-0b18-4132-b458-9271b7866e8a",
            "attributes": {
                "build": "build-finder-binary-2",
                "namespace": "myproject",
                "output_imagestream_tag": "172.30.1.1:5000/myproject/build-finder:latest",
                "source_commit_hash": "",
                "source_repository": ""
            },
            "links": {
                "self": "https://example.com/build_events/e47b77ad-0b18-4132-b458-9271b7866e8a"
            }
        },
        {
            "type": "build_events",
            "id": "b35a637c-3f3e-4e37-9d44-f738a2e209f7",
            "attributes": {
                "build": "git-enricher-binary-1",
                "namespace": "analytics",
                "output_imagestream_tag": "172.30.1.1:5000/analytics/git-enricher:latest",
                "source_commit_hash": "",
                "source_repository": ""
            },
            "links": {
                "self": "https://example.com/build_events/b35a637c-3f3e-4e37-9d44-f738a2e209f7"
            }
        },
        {
            "type": "build_events",
            "id": "01ac4337-bdda-417d-a2ae-2f979522cc1c",
            "attributes": {
                "build": "nodejs-ex-2",
                "namespace": "testing-1",
                "output_imagestream_tag": "172.30.1.1:5000/testing-1/nodejs-ex:latest",
                "source_commit_hash": "3d44de3ba8fef0b2baca4ddd001c0db286ea4cd3",
                "source_repository": "https://github.com/openshift/nodejs-ex.git"
            },
            "links": {
                "self": "https://example.com/build_events/01ac4337-bdda-417d-a2ae-2f979522cc1c"
            }
        },
        {
            "type": "build_events",
            "id": "da4333cc-0f08-44d8-b3e7-5e9fc1acaa2a",
            "attributes": {
                "build": "rest-gw-binary-4",
                "namespace": "analytics",
                "output_imagestream_tag": "172.30.1.1:5000/analytics/rest-gw:latest",
                "source_commit_hash": "",
                "source_repository": ""
            },
            "links": {
                "self": "https://example.com/build_events/da4333cc-0f08-44d8-b3e7-5e9fc1acaa2a"
            }
        },
        {
            "type": "build_events",
            "id": "25c9a712-1d53-416b-a396-b16b7003e4bd",
            "attributes": {
                "build": "nodejs-ex-4",
                "namespace": "testing-1",
                "output_imagestream_tag": "172.30.1.1:5000/testing-1/nodejs-ex:latest",
                "source_commit_hash": "3d44de3ba8fef0b2baca4ddd001c0db286ea4cd3",
                "source_repository": "https://github.com/openshift/nodejs-ex.git"
            },
            "links": {
                "self": "https://example.com/build_events/25c9a712-1d53-416b-a396-b16b7003e4bd"
            }
        },
        {
            "type": "build_events",
            "id": "67bf0045-77df-4c96-8fcc-6d4ca5eb9a55",
            "attributes": {
                "build": "build-finder-binary-13",
                "namespace": "myproject",
                "output_imagestream_tag": "172.30.1.1:5000/myproject/build-finder:latest",
                "source_commit_hash": "",
                "source_repository": ""
            },
            "links": {
                "self": "https://example.com/build_events/67bf0045-77df-4c96-8fcc-6d4ca5eb9a55"
            }
        },
        {
            "type": "build_events",
            "id": "0ec31e7b-d94a-4743-8a7e-2babc25005ac",
            "attributes": {
                "build": "git-commit-finder-binary-4",
                "namespace": "analytics",
                "output_imagestream_tag": "172.30.1.1:5000/analytics/git-commit-finder:latest",
                "source_commit_hash": "",
                "source_repository": ""
            },
            "links": {
                "self": "https://example.com/build_events/0ec31e7b-d94a-4743-8a7e-2babc25005ac"
            }
        },
        {
            "type": "build_events",
            "id": "e515862b-510c-4bbc-a44f-7d9be19d9eeb",
            "attributes": {
                "build": "rest-gw-binary-7",
                "namespace": "analytics",
                "output_imagestream_tag": "172.30.1.1:5000/analytics/rest-gw:latest",
                "source_commit_hash": "",
                "source_repository": ""
            },
            "links": {
                "self": "https://example.com/build_events/e515862b-510c-4bbc-a44f-7d9be19d9eeb"
            }
        },
        {
            "type": "build_events",
            "id": "9de9a7e8-7c61-4ba4-81dc-434962674039",
            "attributes": {
                "build": "oe2k-binary-4",
                "namespace": "myproject",
                "output_imagestream_tag": "172.30.1.1:5000/myproject/oe2k:latest",
                "source_commit_hash": "",
                "source_repository": ""
            },
            "links": {
                "self": "https://example.com/build_events/9de9a7e8-7c61-4ba4-81dc-434962674039"
            }
        },
        {
            "type": "build_events",
            "id": "91dc53de-dd06-4745-a2fe-f2e7c73b136e",
            "attributes": {
                "build": "build-finder-binary-14",
                "namespace": "myproject",
                "output_imagestream_tag": "172.30.1.1:5000/myproject/build-finder:latest",
                "source_commit_hash": "",
                "source_repository": ""
            },
            "links": {
                "self": "https://example.com/build_events/91dc53de-dd06-4745-a2fe-f2e7c73b136e"
            }
        },
        {
            "type": "build_events",
            "id": "d800d0d0-a74f-4896-88b0-9fb3810542ee",
            "attributes": {
                "build": "git-commit-finder-binary-1",
                "namespace": "analytics",
                "output_imagestream_tag": "172.30.1.1:5000/analytics/git-commit-finder:latest",
                "source_commit_hash": "",
                "source_repository": ""
            },
            "links": {
                "self": "https://example.com/build_events/d800d0d0-a74f-4896-88b0-9fb3810542ee"
            }
        },
        {
            "type": "build_events",
            "id": "741c014f-3676-4223-af79-5b90e6c51289",
            "attributes": {
                "build": "build-finder-binary-10",
                "namespace": "myproject",
                "output_imagestream_tag": "172.30.1.1:5000/myproject/build-finder:latest",
                "source_commit_hash": "",
                "source_repository": ""
            },
            "links": {
                "self": "https://example.com/build_events/741c014f-3676-4223-af79-5b90e6c51289"
            }
        },
        {
            "type": "build_events",
            "id": "f42fc4bd-655c-43c6-ad6d-b847a37903d6",
            "attributes": {
                "build": "rest-gw-binary-11",
                "namespace": "analytics",
                "output_imagestream_tag": "172.30.1.1:5000/analytics/rest-gw:latest",
                "source_commit_hash": "",
                "source_repository": ""
            },
            "links": {
                "self": "https://example.com/build_events/f42fc4bd-655c-43c6-ad6d-b847a37903d6"
            }
        },
        {
            "type": "build_events",
            "id": "c3a45c56-2821-406a-91a7-8b18d88ecb56",
            "attributes": {
                "build": "git-enricher-binary-2",
                "namespace": "analytics",
                "output_imagestream_tag": "172.30.1.1:5000/analytics/git-enricher:latest",
                "source_commit_hash": "",
                "source_repository": ""
            },
            "links": {
                "self": "https://example.com/build_events/c3a45c56-2821-406a-91a7-8b18d88ecb56"
            }
        },
        {
            "type": "build_events",
            "id": "b12deff2-e61c-4aad-816a-cc54f20bfcc2",
            "attributes": {
                "build": "oe2k-binary-5",
                "namespace": "myproject",
                "output_imagestream_tag": "172.30.1.1:5000/myproject/oe2k:latest",
                "source_commit_hash": "",
                "source_repository": ""
            },
            "links": {
                "self": "https://example.com/build_events/b12deff2-e61c-4aad-816a-cc54f20bfcc2"
            }
        },
        {
            "type": "build_events",
            "id": "a27c61fd-e074-489b-93ac-ceed769523db",
            "attributes": {
                "build": "oe2k-binary-1",
                "namespace": "myproject",
                "output_imagestream_tag": "172.30.1.1:5000/myproject/oe2k:latest",
                "source_commit_hash": "",
                "source_repository": ""
            },
            "links": {
                "self": "https://example.com/build_events/a27c61fd-e074-489b-93ac-ceed769523db"
            }
        },
        {
            "type": "build_events",
            "id": "73d57a4b-d8d3-466a-a5df-f454754363f6",
            "attributes": {
                "build": "build-finder-binary-17",
                "namespace": "myproject",
                "output_imagestream_tag": "172.30.1.1:5000/myproject/build-finder:latest",
                "source_commit_hash": "",
                "source_repository": ""
            },
            "links": {
                "self": "https://example.com/build_events/73d57a4b-d8d3-466a-a5df-f454754363f6"
            }
        },
        {
            "type": "build_events",
            "id": "9acb582b-e31a-442e-855f-ac0fb1698028",
            "attributes": {
                "build": "git-commit-finder-binary-7",
                "namespace": "analytics",
                "output_imagestream_tag": "172.30.1.1:5000/analytics/git-commit-finder:latest",
                "source_commit_hash": "",
                "source_repository": ""
            },
            "links": {
                "self": "https://example.com/build_events/9acb582b-e31a-442e-855f-ac0fb1698028"
            }
        },
        {
            "type": "build_events",
            "id": "2365b5c8-b99a-44bc-bcb4-8465333b5515",
            "attributes": {
                "build": "rest-gw-binary-12",
                "namespace": "analytics",
                "output_imagestream_tag": "172.30.1.1:5000/analytics/rest-gw:latest",
                "source_commit_hash": "",
                "source_repository": ""
            },
            "links": {
                "self": "https://example.com/build_events/2365b5c8-b99a-44bc-bcb4-8465333b5515"
            }
        },
        {
            "type": "build_events",
            "id": "544a075c-b641-4888-914d-17bb1824a465",
            "attributes": {
                "build": "buildlog2hdfs-binary-1",
                "namespace": "analytics",
                "output_imagestream_tag": "172.30.1.1:5000/analytics/buildlog2hdfs:latest",
                "source_commit_hash": "",
                "source_repository": ""
            },
            "links": {
                "self": "https://example.com/build_events/544a075c-b641-4888-914d-17bb1824a465"
            }
        },
        {
            "type": "build_events",
            "id": "db811f80-6174-4dbe-870e-7f477ee79152",
            "attributes": {
                "build": "git-commit-finder-binary-3",
                "namespace": "analytics",
                "output_imagestream_tag": "172.30.1.1:5000/analytics/git-commit-finder:latest",
                "source_commit_hash": "",
                "source_repository": ""
            },
            "links": {
                "self": "https://example.com/build_events/db811f80-6174-4dbe-870e-7f477ee79152"
            }
        },
        {
            "type": "build_events",
            "id": "32c1ec9d-916a-47ba-95d8-af010019f669",
            "attributes": {
                "build": "rest-gw-binary-10",
                "namespace": "analytics",
                "output_imagestream_tag": "172.30.1.1:5000/analytics/rest-gw:latest",
                "source_commit_hash": "",
                "source_repository": ""
            },
            "links": {
                "self": "https://example.com/build_events/32c1ec9d-916a-47ba-95d8-af010019f669"
            }
        },
        {
            "type": "build_events",
            "id": "900ac4d7-5a8f-4872-a55f-2f8988bf8eb4",
            "attributes": {
                "build": "rest-gw-binary-15",
                "namespace": "analytics",
                "output_imagestream_tag": "172.30.1.1:5000/analytics/rest-gw:latest",
                "source_commit_hash": "",
                "source_repository": ""
            },
            "links": {
                "self": "https://example.com/build_events/900ac4d7-5a8f-4872-a55f-2f8988bf8eb4"
            }
        },
        {
            "type": "build_events",
            "id": "566bebec-0741-456e-8a6c-f739dd405487",
            "attributes": {
                "build": "rest-gw-binary-1",
                "namespace": "myproject",
                "output_imagestream_tag": "172.30.1.1:5000/myproject/rest-gw:latest",
                "source_commit_hash": "",
                "source_repository": ""
            },
            "links": {
                "self": "https://example.com/build_events/566bebec-0741-456e-8a6c-f739dd405487"
            }
        },
        {
            "type": "build_events",
            "id": "aed1aeac-25e6-469d-ab95-74cea34503c6",
            "attributes": {
                "build": "build-finder-binary-1",
                "namespace": "myproject",
                "output_imagestream_tag": "172.30.1.1:5000/myproject/build-finder:latest",
                "source_commit_hash": "",
                "source_repository": ""
            },
            "links": {
                "self": "https://example.com/build_events/aed1aeac-25e6-469d-ab95-74cea34503c6"
            }
        },
        {
            "type": "build_events",
            "id": "169a2d22-d17e-4ac3-a493-f0af2e098f15",
            "attributes": {
                "build": "nodejs-ex-3",
                "namespace": "testing-1",
                "output_imagestream_tag": "172.30.1.1:5000/testing-1/nodejs-ex:latest",
                "source_commit_hash": "3d44de3ba8fef0b2baca4ddd001c0db286ea4cd3",
                "source_repository": "https://github.com/openshift/nodejs-ex.git"
            },
            "links": {
                "self": "https://example.com/build_events/169a2d22-d17e-4ac3-a493-f0af2e098f15"
            }
        },
        {
            "type": "build_events",
            "id": "91bf0e60-684b-4779-a8b7-36fd1c772c24",
            "attributes": {
                "build": "rest-gw-binary-13",
                "namespace": "analytics",
                "output_imagestream_tag": "172.30.1.1:5000/analytics/rest-gw:latest",
                "source_commit_hash": "",
                "source_repository": ""
            },
            "links": {
                "self": "https://example.com/build_events/91bf0e60-684b-4779-a8b7-36fd1c772c24"
            }
        },
        {
            "type": "build_events",
            "id": "d4d67f74-1873-4cef-a748-fbd98c709f43",
            "attributes": {
                "build": "oe2k-binary-9",
                "namespace": "myproject",
                "output_imagestream_tag": "172.30.1.1:5000/myproject/oe2k:latest",
                "source_commit_hash": "",
                "source_repository": ""
            },
            "links": {
                "self": "https://example.com/build_events/d4d67f74-1873-4cef-a748-fbd98c709f43"
            }
        },
        {
            "type": "build_events",
            "id": "f9da15e7-7873-4e48-95dc-77ed9c65f83b",
            "attributes": {
                "build": "build-finder-binary-19",
                "namespace": "myproject",
                "output_imagestream_tag": "172.30.1.1:5000/myproject/build-finder:latest",
                "source_commit_hash": "",
                "source_repository": ""
            },
            "links": {
                "self": "https://example.com/build_events/f9da15e7-7873-4e48-95dc-77ed9c65f83b"
            }
        },
        {
            "type": "build_events",
            "id": "f6e8b091-96a1-450d-88d8-9a54cc3951dc",
            "attributes": {
                "build": "build-finder-binary-1",
                "namespace": "analytics",
                "output_imagestream_tag": "172.30.1.1:5000/analytics/build-finder:latest",
                "source_commit_hash": "",
                "source_repository": ""
            },
            "links": {
                "self": "https://example.com/build_events/f6e8b091-96a1-450d-88d8-9a54cc3951dc"
            }
        },
        {
            "type": "build_events",
            "id": "692d1da9-d6fa-43a6-a9bd-9ef95221744b",
            "attributes": {
                "build": "git-commit-finder-binary-6",
                "namespace": "analytics",
                "output_imagestream_tag": "172.30.1.1:5000/analytics/git-commit-finder:latest",
                "source_commit_hash": "",
                "source_repository": ""
            },
            "links": {
                "self": "https://example.com/build_events/692d1da9-d6fa-43a6-a9bd-9ef95221744b"
            }
        },
        {
            "type": "build_events",
            "id": "c689a29a-292e-4e03-8f33-0b68ffe14e66",
            "attributes": {
                "build": "oe2k-binary-3",
                "namespace": "myproject",
                "output_imagestream_tag": "172.30.1.1:5000/myproject/oe2k:latest",
                "source_commit_hash": "",
                "source_repository": ""
            },
            "links": {
                "self": "https://example.com/build_events/c689a29a-292e-4e03-8f33-0b68ffe14e66"
            }
        },
        {
            "type": "build_events",
            "id": "a3931283-3deb-4fe9-9549-4cbaad798269",
            "attributes": {
                "build": "build-finder-binary-3",
                "namespace": "myproject",
                "output_imagestream_tag": "172.30.1.1:5000/myproject/build-finder:latest",
                "source_commit_hash": "",
                "source_repository": ""
            },
            "links": {
                "self": "https://example.com/build_events/a3931283-3deb-4fe9-9549-4cbaad798269"
            }
        },
        {
            "type": "build_events",
            "id": "4440b089-d27f-4e0c-b4af-63188de8c750",
            "attributes": {
                "build": "rest-gw-binary-2",
                "namespace": "myproject",
                "output_imagestream_tag": "172.30.1.1:5000/myproject/rest-gw:latest",
                "source_commit_hash": "",
                "source_repository": ""
            },
            "links": {
                "self": "https://example.com/build_events/4440b089-d27f-4e0c-b4af-63188de8c750"
            }
        },
        {
            "type": "build_events",
            "id": "a7623456-d1d0-460a-b433-a90ca0242424",
            "attributes": {
                "build": "build-finder-binary-6",
                "namespace": "myproject",
                "output_imagestream_tag": "172.30.1.1:5000/myproject/build-finder:latest",
                "source_commit_hash": "",
                "source_repository": ""
            },
            "links": {
                "self": "https://example.com/build_events/a7623456-d1d0-460a-b433-a90ca0242424"
            }
        },
        {
            "type": "build_events",
            "id": "b39ac1ff-6112-4e4b-b91c-8d8dc0b45e14",
            "attributes": {
                "build": "rest-gw-binary-3",
                "namespace": "myproject",
                "output_imagestream_tag": "172.30.1.1:5000/myproject/rest-gw:latest",
                "source_commit_hash": "",
                "source_repository": ""
            },
            "links": {
                "self": "https://example.com/build_events/b39ac1ff-6112-4e4b-b91c-8d8dc0b45e14"
            }
        },
        {
            "type": "build_events",
            "id": "87fe6e49-430f-4bce-ae6f-f0006d56cd8a",
            "attributes": {
                "build": "oe2k-binary-7",
                "namespace": "myproject",
                "output_imagestream_tag": "172.30.1.1:5000/myproject/oe2k:latest",
                "source_commit_hash": "",
                "source_repository": ""
            },
            "links": {
                "self": "https://example.com/build_events/87fe6e49-430f-4bce-ae6f-f0006d56cd8a"
            }
        }
    ]
}