{
    "metadata": {
        "name": "build-finder-binary-10-build",
        "namespace": "myproject",
        "selfLink": "/api/v1/namespaces/myproject/pods/build-finder-binary-10-build",
        "uid": "ecc674e0-3b81-11e7-a1fa-54ee7504b46f",
        "resourceVersion": "4448",
        "creationTimestamp": "2017-05-18T04:24:46Z",
        "labels": {
            "openshift.io/build.name": "build-finder-binary-10"
        },
        "annotations": {
            "openshift.io/build.name": "build-finder-binary-10",
            "openshift.io/scc": "privileged"
        }
    },
    "spec": {
        "volumes": [
            {
                "name": "docker-socket",
                "hostPath": {
                    "path": "/var/run/docker.sock"
                }
            },
            {
                "name": "builder-dockercfg-w56br-push",
                "secret": {
                    "secretName": "builder-dockercfg-w56br",
                    "defaultMode": 420
                }
            },
            {
                "name": "builder-token-dd4nl",
                "secret": {
                    "secretName": "builder-token-dd4nl",
                    "defaultMode": 420
                }
            }
        ],
        "containers": [
            {
                "name": "docker-build",
                "image": "registry.access.redhat.com/openshift3/ose-docker-builder:v3.5.5.8",
                "args": [
                    "--loglevel=0"
                ],
                "env": [
                    {
                        "name": "BUILD",
                        "value": "{\"kind\":\"Build\",\"apiVersion\":\"v1\",\"metadata\":{\"name\":\"build-finder-binary-10\",\"namespace\":\"myproject\",\"selfLink\":\"/oapi/v1/namespaces/myproject/builds/build-finder-binary-10\",\"uid\":\"ecc31cce-3b81-11e7-a1fa-54ee7504b46f\",\"resourceVersion\":\"4447\",\"creationTimestamp\":\"2017-05-18T04:24:46Z\",\"labels\":{\"build\":\"build-finder-binary\",\"buildconfig\":\"build-finder-binary\",\"openshift.io/build-config.name\":\"build-finder-binary\",\"openshift.io/build.start-policy\":\"Serial\"},\"annotations\":{\"openshift.io/build-config.name\":\"build-finder-binary\",\"openshift.io/build.number\":\"10\"}},\"spec\":{\"serviceAccount\":\"builder\",\"source\":{\"type\":\"Binary\",\"binary\":{}},\"revision\":{\"type\":\"Git\",\"git\":{\"commit\":\"4d7276c1aae66a13620f5d36dcbfd546c4c1f3c9\",\"author\":{\"name\":\"Christoph Görn\",\"email\":\"goern@redhat.com\"},\"committer\":{\"name\":\"Christoph Görn\",\"email\":\"goern@redhat.com\"},\"message\":\"restructured the directory layout and added a Makefile\"}},\"strategy\":{\"type\":\"Docker\",\"dockerStrategy\":{\"from\":{\"kind\":\"DockerImage\",\"name\":\"registry.access.redhat.com/rhel7-atomic@sha256:d562d488b9d2c0be55f81e33ed8d3c277533631d31f93d790903c63468006003\"}}},\"output\":{\"to\":{\"kind\":\"DockerImage\",\"name\":\"172.30.1.1:5000/myproject/build-finder:latest\"},\"pushSecret\":{\"name\":\"builder-dockercfg-w56br\"}},\"resources\":{},\"postCommit\":{},\"nodeSelector\":null,\"triggeredBy\":null},\"status\":{\"phase\":\"New\",\"outputDockerImageReference\":\"172.30.1.1:5000/myproject/build-finder:latest\",\"config\":{\"kind\":\"BuildConfig\",\"namespace\":\"myproject\",\"name\":\"build-finder-binary\"},\"output\":{}}}\n"
                    },
                    {
                        "name": "ORIGIN_VERSION",
                        "value": "v3.5.5.8"
                    },
                    {
                        "name": "PUSH_DOCKERCFG_PATH",
                        "value": "/var/run/secrets/openshift.io/push"
                    }
                ],
                "resources": {},
                "volumeMounts": [
                    {
                        "name": "docker-socket",
                        "mountPath": "/var/run/docker.sock"
                    },
                    {
                        "name": "builder-dockercfg-w56br-push",
                        "readOnly": true,
                        "mountPath": "/var/run/secrets/openshift.io/push"
                    },
                    {
                        "name": "builder-token-dd4nl",
                        "readOnly": true,
                        "mountPath": "/var/run/secrets/kubernetes.io/serviceaccount"
                    }
                ],
                "terminationMessagePath": "/dev/termination-log",
                "imagePullPolicy": "IfNotPresent",
                "securityContext": {
                    "privileged": true
                },
                "stdin": true,
                "stdinOnce": true
            }
        ],
        "restartPolicy": "Never",
        "terminationGracePeriodSeconds": 30,
        "dnsPolicy": "ClusterFirst",
        "serviceAccountName": "builder",
        "serviceAccount": "builder",
        "securityContext": {},
        "imagePullSecrets": [
            {
                "name": "builder-dockercfg-w56br"
            }
        ]
    },
    "status": {
        "phase": "Pending"
    }
}