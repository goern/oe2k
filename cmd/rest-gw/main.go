/*
   oe2k
   Copyright (C) 2017 Christoph Görn

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strings"

	"github.com/Shopify/sarama"
	log "github.com/Sirupsen/logrus"
	"github.com/google/jsonapi"
	"github.com/gorilla/mux"
	"github.com/spf13/viper"
	"gitlab.com/goern/oe2k/pkg"
	"gitlab.com/goern/oe2k/pkg/brain"
)

var builds *brain.InMemoryBrain

//healthzHandler is for liveness and readyness checks
func healthzHandler() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// TODO include kafka connection status check
		// TODO include info if we could talk to kubernetes master

		fmt.Fprintf(w, "OK")
	})
}

func buildsEndpoint(w http.ResponseWriter, req *http.Request) {
	var dump []*oe2k.BuildEvent

	it := builds.Builds.Iterator()

	for elem := range it.C {
		dump = append(dump, elem.(*oe2k.BuildEvent))
	}

	jsonapiRuntime := jsonapi.NewRuntime().Instrument("builds.list")

	w.Header().Set("Content-Type", jsonapi.MediaType)
	// w.WriteHeader(http.StatusOK)

	if err := jsonapiRuntime.MarshalManyPayload(w, dump); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func init() {
	viper.SetDefault("rest-api-port", "8080")
	viper.SetDefault("kafka-peers", "localhost:9092")
	viper.SetDefault("verbose", false)

	viper.SetConfigType("yaml")
	viper.SetEnvPrefix("oe2k")

	viper.BindEnv("rest_api_port")
	viper.BindEnv("kafka_peers")
	viper.BindEnv("verbose")

	if viper.GetBool("verbose") {
		log.SetLevel(log.DebugLevel)
	}

	builds = brain.NewInMemoryBrain()

}

func main() {
	brokerList := strings.Split(viper.GetString("kafka_peers"), ",")
	log.Infof("Kafka brokers: %s", strings.Join(brokerList, ", "))

	consumer, err := sarama.NewConsumer(brokerList, nil)
	if err != nil {
		log.Errorln("Could not create consumer: ", err)
	}

	handleDiscoveredBuilds("openshift_builds_discovered", consumer)
	handleGitCommitHashes("openshift_builds_git_commit_hash", consumer)

	router := mux.NewRouter()

	router.HandleFunc("/builds", buildsEndpoint).Methods("GET")
	router.Handle("/_healthz", healthzHandler())

	log.Fatal(http.ListenAndServe(":"+viper.GetString("rest_api_port"), router))

}

func handleDiscoveredBuilds(topic string, consumer sarama.Consumer) {
	partitionList, err := consumer.Partitions(topic)
	if err != nil {
		log.Errorln("Error retrieving partitionList ", err)
	}
	initialOffset := sarama.OffsetOldest

	for _, partition := range partitionList {
		pc, _ := consumer.ConsumePartition(topic, partition, initialOffset)

		go func(pc sarama.PartitionConsumer) {
			for message := range pc.Messages() {

				var build oe2k.BuildEvent
				err := json.Unmarshal(message.Value, &build)
				if err != nil {
					log.Error("error:", err)
				}

				log.Debugf("Build %s: %v", build.UUID, build)

				builds.AddBuild(&build)

			} // for each message
		}(pc) // go func()
	} // for each partition
} // func handleDiscoveredBuilds()

func handleGitCommitHashes(topic string, consumer sarama.Consumer) {
	partitionList, err := consumer.Partitions(topic)
	if err != nil {
		log.Errorln("Error retrieving partitionList ", err)
	}
	initialOffset := sarama.OffsetOldest

	for _, partition := range partitionList {
		pc, _ := consumer.ConsumePartition(topic, partition, initialOffset)

		go func(pc sarama.PartitionConsumer) {
			for message := range pc.Messages() {
				var bsch oe2k.BuildSourceCommitHash
				err := json.Unmarshal(message.Value, &bsch)
				if err != nil {
					log.Error("error:", err)
				}

				builds.SetSourceCommitHash(bsch.UUID, bsch.SourceCommitHash)
				builds.SetSourceRepository(bsch.UUID, bsch.SourceRepository)

			} // for each message
		}(pc) // go func()
	} // for each partition
} // func handleGitCommitHashes()
