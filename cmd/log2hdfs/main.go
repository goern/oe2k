/*
   oe2k
   Copyright (C) 2017 Christoph Görn

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

package main

import (
	"crypto/tls"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"strings"

	"gitlab.com/goern/oe2k/pkg"

	"github.com/Shopify/sarama"
	"github.com/colinmarc/hdfs"

	log "github.com/Sirupsen/logrus"
	"github.com/spf13/viper"
)

var (
	addr = ":8080"
)

func HealthzHandler() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// TODO include kafka connection status check
		// TODO include info if we could talk to HDFS

		fmt.Fprintf(w, "OK")
	})
}

func init() {
	viper.SetDefault("kafka-peers", "localhost:9092")
	viper.SetDefault("hdfs-namenode", "namenode:8082")

	viper.SetDefault("verbose", false)

	viper.SetConfigType("yaml")
	viper.SetEnvPrefix("oe2k")

	viper.BindEnv("kafka_peers")
	viper.BindEnv("hdfs_namenode")
	viper.BindEnv("verbose")

	if viper.GetBool("verbose") {
		log.SetLevel(log.DebugLevel)
	}
}

func main() {
	brokerList := strings.Split(viper.GetString("kafka_peers"), ",")
	log.Infof("Kafka brokers: %s", strings.Join(brokerList, ", "))

	consumer, err := sarama.NewConsumer(brokerList, nil)
	if err != nil {
		log.Errorln("Could not create consumer: ", err)
	}

	hdfsClient, err := hdfs.New(viper.GetString("hdfs_namenode"))
	if err != nil {
		log.Errorln("Could not create HDFS client: ", err)
	}

	subscribe("openshift_builds_discovered", consumer, hdfsClient)

	http.Handle("/_healthz", HealthzHandler())

	log.Fatal(http.ListenAndServe(":8080", nil)) // TODO make this config'able via ENV

}

func subscribe(topic string, consumer sarama.Consumer, client *hdfs.Client) {
	partitionList, err := consumer.Partitions(topic) // 	get all partitions on the given topic
	if err != nil {
		log.Errorln("Error retrieving partitionList ", err)
	}
	initialOffset := sarama.OffsetOldest

	for _, partition := range partitionList {
		pc, _ := consumer.ConsumePartition(topic, partition, initialOffset)

		go func(pc sarama.PartitionConsumer) {
			logfile, _ := client.Create("/build_events.log")

			logger := log.New()
			logger.Out = logfile

			for message := range pc.Messages() {
				var build oe2k.BuildEvent
				err := json.Unmarshal(message.Value, &build)
				if err != nil {
					log.Error("error:", err)
				}

				log.Debugf("Received BuildEvent: %v", build)
				getBuildLog(build.Namespace, build.Build, build.UUID, client)

			} // for each message
		}(pc) // go func()
	} // for each partition
} // func subscribe()

//TODO DRY oe2k.GetBearerToken() (string, error)
func getBuildLog(namespace string, build string, uuid string, hdfsClient *hdfs.Client) {
	req, err := http.NewRequest("GET", "https://openshift.default.svc.cluster.local/oapi/v1/namespaces/"+namespace+"/builds/"+build+"/log", nil)
	if err != nil {
		log.Fatal(err)
		return
	}

	token, err := oe2k.GetBearerToken()
	if err != nil {
		return
	}

	log.Debugf("token=%s", token)

	req.Header.Set("Authorization", "Bearer "+token)
	req.Header.Set("User-Agent", "buildlog2hdfs/v0.0.1 openshift/v1.5 (linux/amd64)")
	req.Header.Set("Accept", "application/json, */*")

	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}

	client := &http.Client{Transport: tr}

	resp, err := client.Do(req)
	if err != nil {
		log.Fatal(err)
		return
	}
	defer resp.Body.Close()

	log.Debug(resp)

	out, err := hdfsClient.Create("/" + build + "_" + uuid + ".log")
	if err != nil {
		log.Fatal(err)
	}
	defer out.Close()

	io.Copy(out, resp.Body)
}
