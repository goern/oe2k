/*
   oe2k
   Copyright (C) 2017 Christoph Görn

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strings"
	"time"

	"github.com/Shopify/sarama"
	log "github.com/Sirupsen/logrus"
	"github.com/gorilla/mux"
	"github.com/spf13/viper"
	oe2k "gitlab.com/goern/oe2k/pkg"
)

var builds []*oe2k.BuildEvent
var producer *sarama.AsyncProducer

//healthzHandler is for liveness and readyness checks
func healthzHandler() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// TODO include kafka connection status check
		// TODO include info if we could talk to kubernetes master

		fmt.Fprintf(w, "OK")
	})
}

func init() {
	viper.SetDefault("my-port", "8080")
	viper.SetDefault("kafka-peers", "localhost:9092")
	viper.SetDefault("verbose", false)

	viper.SetConfigType("yaml")
	viper.SetEnvPrefix("oe2k")

	viper.BindEnv("my_port")
	viper.BindEnv("kafka_peers")
	viper.BindEnv("verbose")

	if viper.GetBool("verbose") {
		log.SetLevel(log.DebugLevel)
	}
}

func main() {
	brokerList := strings.Split(viper.GetString("kafka_peers"), ",")
	log.Infof("Kafka brokers: %s", strings.Join(brokerList, ", "))

	consumer, err := sarama.NewConsumer(brokerList, nil)
	if err != nil {
		log.Errorln("Could not create consumer: ", err)
	}

	config := sarama.NewConfig()
	config.Producer.RequiredAcks = sarama.WaitForLocal
	config.Producer.Flush.Frequency = 500 * time.Millisecond
	config.Producer.Compression = sarama.CompressionNone

	producer, err := sarama.NewAsyncProducer(brokerList, config)
	if err != nil {
		log.Fatal("Failed to start Sarama producer:", err)
	}

	go func() {
		for err := range producer.Errors() {
			log.Println("Failed to write event:", err)
		}
	}()

	handleTopic("openshift_builds_discovered", consumer, producer)

	router := mux.NewRouter()

	router.Handle("/_healthz", healthzHandler())

	log.Fatal(http.ListenAndServe(":"+viper.GetString("my_port"), router))

}

func handleTopic(topic string, consumer sarama.Consumer, producer sarama.AsyncProducer) {
	partitionList, err := consumer.Partitions(topic)
	if err != nil {
		log.Errorln("Error retrieving partitionList ", err)
	}
	initialOffset := sarama.OffsetOldest

	for _, partition := range partitionList {
		pc, _ := consumer.ConsumePartition(topic, partition, initialOffset)

		go func(pc sarama.PartitionConsumer) {
			for message := range pc.Messages() {

				var build oe2k.BuildEvent
				err := json.Unmarshal(message.Value, &build)
				if err != nil {
					log.Error("error:", err)
				}

				log.Debugf("Build %s: %v", build.UUID, build.Build)

				buildlog, err := oe2k.GetBuildLog(build.Namespace, build.Build)
				if err != nil {
					log.Fatal(err)
				} else {
					hash, err := oe2k.FindGitCommit(strings.NewReader(*buildlog))
					if err != nil {
						log.Info(err)
					} else {
						repo, _ := oe2k.FindGitRepository(strings.NewReader(*buildlog)) // FIXME this (creating a new Reader from string) is inefficient isnt it?

						gitCommitHash := oe2k.BuildSourceCommitHash{UUID: build.UUID, SourceCommitHash: hash, SourceRepository: repo}

						log.Debug(gitCommitHash)

						gitCommitHashString, err := json.Marshal(gitCommitHash)
						if err != nil {
							log.Error(err)
							return
						}

						producer.Input() <- &sarama.ProducerMessage{
							Topic: "openshift_builds_git_commit_hash",
							Key:   sarama.StringEncoder(build.UUID),
							Value: sarama.StringEncoder(gitCommitHashString),
						}
					}
				}

			} // for each message
		}(pc) // go func()
	} // for each partition
} // func subscribe()
