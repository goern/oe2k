/*
   oe2k
   Copyright (C) 2017 Christoph Görn

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strings"
	"time"

	"gitlab.com/goern/oe2k/pkg"

	"k8s.io/client-go/pkg/api/v1"

	"github.com/Shopify/sarama"
	"github.com/prometheus/client_golang/prometheus"

	log "github.com/Sirupsen/logrus"
	"github.com/spf13/viper"
)

var (
	openShiftSucceededBuilds = prometheus.NewCounter(prometheus.CounterOpts{
		Name: "openshift_succeeded_builds",
		Help: "The number of successful OpenShift Builds observed",
	})
)

var producer *sarama.AsyncProducer

//HealthzHandler is for liveness and readyness checks
func HealthzHandler() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// TODO include kafka connection status check
		// TODO include info if we could talk to kubernetes master

		fmt.Fprintf(w, "OK")
	})
}

func init() {
	viper.SetDefault("build-finder-port", ":8080")
	viper.SetDefault("kafka-peers", "localhost:9092")
	viper.SetDefault("verbose", false)

	viper.SetConfigType("yaml")
	viper.SetEnvPrefix("oe2k")

	viper.BindEnv("build_finder_port")
	viper.BindEnv("kafka_peers")
	viper.BindEnv("verbose")

	if viper.GetBool("verbose") {
		log.SetLevel(log.DebugLevel)
	}

	prometheus.MustRegister(openShiftSucceededBuilds)

}

func main() {
	brokerList := strings.Split(viper.GetString("kafka_peers"), ",")
	log.Infof("Kafka brokers: %s", strings.Join(brokerList, ", "))

	consumer, err := sarama.NewConsumer(brokerList, nil)
	if err != nil {
		log.Errorln("Could not create consumer: ", err)
	}

	config := sarama.NewConfig()
	config.Producer.RequiredAcks = sarama.WaitForLocal
	config.Producer.Flush.Frequency = 500 * time.Millisecond
	config.Producer.Compression = sarama.CompressionNone

	producer, err := sarama.NewAsyncProducer(brokerList, config)
	if err != nil {
		log.Fatal("Failed to start Sarama producer:", err)
	}

	go func() {
		for err := range producer.Errors() {
			log.Println("Failed to write event:", err)
		}
	}()

	subscribe("openshift_events", consumer, producer)

	http.Handle("/metrics", prometheus.Handler())
	http.Handle("/_healthz", HealthzHandler())

	log.Fatal(http.ListenAndServe(":"+viper.GetString("build_finder_port"), nil))

}

func subscribe(topic string, consumer sarama.Consumer, producer sarama.AsyncProducer) {
	partitionList, err := consumer.Partitions(topic) // 	get all partitions on the given topic
	if err != nil {
		log.Errorln("Error retrieving partitionList ", err)
	}
	initialOffset := sarama.OffsetOldest

	for _, partition := range partitionList {
		pc, _ := consumer.ConsumePartition(topic, partition, initialOffset)

		go func(pc sarama.PartitionConsumer) {
			for message := range pc.Messages() {

				var pod v1.Pod
				err := json.Unmarshal(message.Value, &pod)
				if err != nil {
					log.Error("error:", err)
				}

				log.Debugf("Event pod %s: state: %s", pod.GetName(), pod.Status.Phase)

				// check if the pod is a build and if it was successful
				if strings.HasSuffix(pod.GetName(), "build") {
					if pod.Status.Phase == v1.PodSucceeded {
						nsl := strings.Split(pod.GetName(), "-")
						bn := strings.Join(nsl[:len(nsl)-1], "-")

						b := oe2k.NewBuildEvent()
						b.Build = bn
						b.Namespace = pod.GetNamespace()

						for _, env := range pod.Spec.Containers[0].Env {
							if strings.EqualFold(env.Name, "BUILD") {
								var build interface{}

								err := json.Unmarshal([]byte(env.Value), &build)
								if err != nil {
									log.Error("error:", err)
								}

								// This is hacky, and all just to avoid vendoring openshift/origin
								for _, v := range build.(map[string]interface{}) {
									switch t := v.(type) {
									case map[string]interface{}:
										if t["outputDockerImageReference"] != nil {
											b.OutputImageStreamTag = string(t["outputDockerImageReference"].(string))
										}
									}
								}

							} // if BUILD
						} // for each ENV

						openshiftBuildJSONString, err := json.Marshal(b)
						if err != nil {
							log.Error(err)
							return
						}

						producer.Input() <- &sarama.ProducerMessage{
							Topic: "openshift_builds_discovered",
							Key:   sarama.StringEncoder(bn),
							Value: sarama.StringEncoder(openshiftBuildJSONString),
						}

						openShiftSucceededBuilds.Inc()
					} // if succeeded
				} // if build
			} // for each message
		}(pc) // go func()
	} // for each partition
} // func subscribe()
