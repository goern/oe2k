/*
   oe2k
   Copyright (C) 2017 Christoph Görn

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"strings"
	"syscall"
	"time"

	"github.com/Shopify/sarama"
	log "github.com/Sirupsen/logrus"
	"github.com/prometheus/client_golang/prometheus"

	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/pkg/api/v1"
	"k8s.io/client-go/pkg/fields"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/cache"
)

var (
	port    = os.Getenv("PORT")
	brokers = os.Getenv("KAFKA_PEERS")

	openShiftPodEvents = prometheus.NewCounter(prometheus.CounterOpts{
		Name: "openshift_pod_events_total",
		Help: "The number of OpenShift Pod events",
	})
	openShiftPodCreateEvents = prometheus.NewCounter(prometheus.CounterOpts{
		Name: "openshift_pod_create_events_total",
		Help: "The number of OpenShift add/create events",
	})
	openShiftPodDeleteEvents = prometheus.NewCounter(prometheus.CounterOpts{
		Name: "openshift_pod_delete_events_total",
		Help: "The number of OpenShift delete events",
	})
	openShiftPodUpdateEvents = prometheus.NewCounter(prometheus.CounterOpts{
		Name: "openshift_pod_update_events_total",
		Help: "The number of OpenShift update events",
	})
)

type Server struct {
	OpenShiftEventProducer sarama.AsyncProducer
}

func (s *Server) HealthzHandler() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// TODO include kafka connection status check
		// TODO include info if we could talk to kubernetes master

		fmt.Fprintf(w, "OK")
	})
}

func (s *Server) Run(port string) error {
	config, err := rest.InClusterConfig()
	if err != nil {
		log.Fatal(err)
		return err
	}

	clientset, err := kubernetes.NewForConfig(config)
	if err != nil {
		log.Fatal(err)
		return err
	}

	watchlist := cache.NewListWatchFromClient(clientset.Core().RESTClient(), "pods", v1.NamespaceAll, fields.Everything())
	_, controller := cache.NewInformer(
		watchlist,
		&v1.Pod{},
		time.Second*0,
		cache.ResourceEventHandlerFuncs{ // TODO this could be done more generic for all objects
			AddFunc: func(obj interface{}) {
				openshiftObjectJSONString, err := json.Marshal(obj)
				if err != nil {
					log.Error(err)
					return
				}

				s.OpenShiftEventProducer.Input() <- &sarama.ProducerMessage{
					Topic:     "openshift_events",
					Key:       sarama.StringEncoder(obj.(*v1.Pod).GetName()),
					Value:     sarama.StringEncoder(openshiftObjectJSONString),
					Partition: 3,
				}

				openShiftPodCreateEvents.Inc()
				openShiftPodEvents.Inc()
			},
			DeleteFunc: func(obj interface{}) {
				openshiftObjectJSONString, err := json.Marshal(obj)
				if err != nil {
					log.Error(err)
					return
				}

				s.OpenShiftEventProducer.Input() <- &sarama.ProducerMessage{
					Topic:     "openshift_events",
					Key:       sarama.StringEncoder(obj.(*v1.Pod).GetName()),
					Value:     sarama.StringEncoder(openshiftObjectJSONString),
					Partition: 1,
				}

				openShiftPodDeleteEvents.Inc()
				openShiftPodEvents.Inc()
			},
			UpdateFunc: func(oldObj, newObj interface{}) {
				openshiftObjectJSONString1, err := json.Marshal(oldObj)
				if err != nil {
					log.Error(err)
					return
				}
				openshiftObjectJSONString2, err := json.Marshal(newObj)
				if err != nil {
					log.Error(err)
					return
				}

				s.OpenShiftEventProducer.Input() <- &sarama.ProducerMessage{
					Topic:     "openshift_events",
					Key:       sarama.StringEncoder(oldObj.(*v1.Pod).GetName()),
					Value:     sarama.StringEncoder(openshiftObjectJSONString1),
					Partition: 2,
				}
				s.OpenShiftEventProducer.Input() <- &sarama.ProducerMessage{
					Topic:     "openshift_events",
					Key:       sarama.StringEncoder(newObj.(*v1.Pod).GetName()),
					Value:     sarama.StringEncoder(openshiftObjectJSONString2),
					Partition: 2,
				}

				openShiftPodUpdateEvents.Inc()
				openShiftPodEvents.Inc()
			},
		},
	)

	stop := make(chan struct{})
	go controller.Run(stop)

	http.Handle("/metrics", prometheus.Handler())
	http.Handle("/_healthz", s.HealthzHandler())

	log.Printf("Listening for requests on %s...", port)
	return http.ListenAndServe(":"+port, nil)
}

func (s *Server) close() error {
	if err := s.OpenShiftEventProducer.Close(); err != nil {
		log.Println("Failed to shut down event log producer cleanly", err)
	}

	return nil
}

func init() {
	prometheus.MustRegister(openShiftPodEvents)
	prometheus.MustRegister(openShiftPodCreateEvents)
	prometheus.MustRegister(openShiftPodDeleteEvents)
	prometheus.MustRegister(openShiftPodUpdateEvents)

}

func main() {
	log.SetLevel(log.DebugLevel)

	brokerList := strings.Split(brokers, ",")
	log.Printf("Kafka brokers: %s", strings.Join(brokerList, ", "))

	server := &Server{
		OpenShiftEventProducer: newOpenShiftEventProducer(brokerList),
	}

	defer func() {
		if err := server.close(); err != nil {
			log.Println("Failed to close server", err)
		}
	}()

	log.Fatal(server.Run(port))

	exitChannel := make(chan os.Signal)
	signal.Notify(exitChannel, syscall.SIGINT, syscall.SIGTERM, syscall.SIGQUIT)
	exitSignal := <-exitChannel
	log.WithFields(log.Fields{"signal": exitSignal}).Infof("Caught %s signal, exiting", exitSignal)
}

func newOpenShiftEventProducer(brokerList []string) sarama.AsyncProducer {
	config := sarama.NewConfig()

	config.Producer.RequiredAcks = sarama.WaitForLocal       // Only wait for the leader to ack
	config.Producer.Flush.Frequency = 500 * time.Millisecond // Flush batches every 500ms
	config.Producer.Compression = sarama.CompressionNone

	producer, err := sarama.NewAsyncProducer(brokerList, config)
	if err != nil {
		log.Fatalln("Failed to start Sarama producer:", err)
	}

	// We will just log to STDOUT if we're not able to produce messages.
	// Note: messages will only be returned here after all retry attempts are exhausted.
	go func() {
		for err := range producer.Errors() {
			log.Println("Failed to write event:", err)
		}
	}()

	return producer
}
